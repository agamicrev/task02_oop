/*
 * Copyright (c) Vertsimaha Systems, Inc.
 * This is the Menu for home task.
 */
package com.vertsimaha;

import com.vertsimaha.menu.Menu;

/**
 * This class Main for home task.
 */
public final class Main {

  /**
   * This is constructor.
   */
  private Main() {
  }

  /**
   * Method which contains Menu.
   * @param args - default
   */
  public static void main(final String[] args) {
    Menu menu = new Menu();
    menu.menu();
  }
}
