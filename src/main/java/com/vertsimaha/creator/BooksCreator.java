/*
 * Copyright (c) Vertsimaha Systems, Inc.
 * This software is part of home task
 */
package com.vertsimaha.creator;

import com.vertsimaha.books.Fiction;
import com.vertsimaha.books.Book;
import com.vertsimaha.books.Lightfiction;
import com.vertsimaha.books.Nonfiction;
import java.util.ArrayList;
import java.util.List;

/**
 * This class was created for creating books.
 *
 * @author Andrii Vertsimaha
 * @version 1.0
 * @since 2018-11-14
 */
public class BooksCreator {

  /**
   * List for containing books.
   */
  private List<Book> books;

  public BooksCreator() {
    this.books = createBooks();
  }

  public final List<Book> getBooks() {
    return books;
  }

  private List<Book> createBooks() {
    books = new ArrayList<Book>();
    books.add(new Nonfiction("EN", "Chemistry", "M.Bolton",
        350, 4788, false));
    books.add(new Fiction("EN", "Harry Potter 1", "J.Rowling",
        450, 1248, 9));
    books.add(new Nonfiction("PL", "Super", "S.Lem",
        200, 2325, true));
    books.add(new Nonfiction("LT", "Mathematics", "D.Petrina",
        50, 4542, false));
    books.add(new Nonfiction("EN", "Programming", "H.Shield",
        1150, 1221, false));
    books.add(new Lightfiction("UA", "Kobzar", "T.Shevchenko",
        450, 1777));
    books.add(new Lightfiction("UA", "Hiba revut voly", "T.Shevchenko",
        150, 1778));
    books.add(new Lightfiction("UA", "Childhood", "T.Shevchenko",
        350, 1123));
    books.add(new Lightfiction("UA", "Mother", "T.Shevchenko",
        150, 1777));
    books.add(new Lightfiction("UA", "Kamenyar", "I.Franko",
        240, 1222));
    books.add(new Lightfiction("UA", "Zahar Berkut", "I.Franko",
        120, 8885));
    books.add(new Lightfiction("UA", "Moses", "I.Franko",
        50, 7854));
    books.add(new Lightfiction("UA", "Lys Mukuta", "I.Franko",
        180, 1733));
    books.add(new Lightfiction("EN", "Harry Potter 2", "J.Rowling",
        140, 2277));
    books.add(new Lightfiction("EN", "Harry Potter 3", "J.Rowling",
        165, 4477));
    books.add(new Fiction("EN", "Harry Potter 4", "J.Rowling",
        350, 5557, 8));
    return books;
  }

}
