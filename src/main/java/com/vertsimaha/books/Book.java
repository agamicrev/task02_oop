/*
 * Copyright (c) Vertsimaha Systems, Inc.
 * This software is part of home task
 */
package com.vertsimaha.books;

/**
 * This class was created for implement abstract class Book.
 *
 * @author Andrii Vertsimaha
 * @version 1.0
 * @since 2018-11-14
 */
public abstract class Book {

  /**
   * Variable for containing language of book.
   */
  private String language;

  /**
   * Variable for containing title of book.
   */
  private String title;

  /**
   * Variable for containing author of book.
   */
  private String author;

  /**
   * Variable for containing number of pages of book.
   */
  private int pages;

  /**
   * Variable for containing ISBN number of book.
   */
  private double isbn;


  /**
   * Constructor goes here.
   *
   * @param language - language of book
   * @param title - title of book
   * @param author - author of book
   * @param pages - number of pages of book
   * @param isbn - ISBN number of book
   */
  public Book(final String language, final String title,
      final String author, final int pages, final double isbn) {
    this.language = language;
    this.title = title;
    this.author = author;
    this.pages = pages;
    this.isbn = isbn;
  }

  /**
   * getter for language.
   */
  public final String getLanguage() {
    return language;
  }

  /**
   * getter for title.
   */
  public final String getTitle() {
    return title;
  }

  /**
   * getter for author.
   */
  public final String getAuthor() {
    return author;
  }

  /**
   * getter for pages.
   */
  public final int getPages() {
    return pages;
  }

  /**
   * getter for isbn.
   */
  public final double getIsbn() {
    return isbn;
  }

  /**
   * abstract method for showing parameters.
   */
  public abstract void showParameters();

  @Override
  public String toString() {
    return " language: " + language
        + ", title: " + title
        + ", author: " + author
        + ", pages: " + pages
        + ", isbn: " + isbn;
  }
}
