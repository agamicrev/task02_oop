/*
 * Copyright (c) Vertsimaha Systems, Inc.
 * This software is part of home task
 */
package com.vertsimaha.books;

/**
 * This class was created for implement class Nonfiction. Nonfiction
 * is a subclass of class Book.
 *
 * @author Andrii Vertsimaha
 * @version 1.0
 * @since 2018-11-14
 */
public class Nonfiction extends Book {

  /**
   * Variable for containing value if a book is history book.
   */
  private boolean history;

  /**
   * Constructor goes here.
   *
   * @param language - language of book
   * @param title - title of book
   * @param author - author of book
   * @param pages - number of pages of book
   * @param isbn - ISBN number of book
   * @param history - is book history book
   */
  public Nonfiction(final String language, final String title,
      final String author, final int pages, final double isbn,
      final boolean history) {
    super(language, title, author, pages, isbn);
    this.history = history;
  }

  /**
   * getter for history.
   */
  public final boolean isHistory() {
    return history;
  }

  @Override
  public final void showParameters() {
    System.out.println(toString());
  }

  @Override
  public final String toString() {
    return "Nonfiction: " + super.toString()
        + ", history=" + history;
  }
}
