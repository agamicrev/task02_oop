/*
 * Copyright (c) Vertsimaha Systems, Inc.
 * This software is part of home task
 */
package com.vertsimaha.books;

/**
 * This class was created for implement class Lightfiction. Lightfiction is a subclass of class
 * Book
 *
 * @author Andrii Vertsimaha
 * @version 1.0
 * @since 2018-11-14
 */
public class Lightfiction extends Book {

  /**
   * Constructor goes here.
   *
   * @param language - language of book
   * @param title - title of book
   * @param author - author of book
   * @param pages - number of pages of book
   * @param isbn - ISBN number of book
   */
  public Lightfiction(final String language, final String title,
      final String author, final int pages, final double isbn) {
    super(language, title, author, pages, isbn);
  }

  @Override
  public final void showParameters() {
    System.out.println(toString());
  }

  @Override
  public final String toString() {
    return "Lightfiction: " + super.toString();
  }

}
