/*
 * Copyright (c) Vertsimaha Systems, Inc.
 * This software is part of home task
 */
package com.vertsimaha.books;

/**
 * This class was created for implement class Fiction. Fiction is a subclass of class Book
 *
 * @author Andrii Vertsimaha
 * @version 1.0
 * @since 2018-11-14
 */
public class Fiction extends Book {

  /**
   * Variable for containing rating of book.
   */
  private int rating;

  /**
   * Constructor goes here.
   *
   * @param language - language of book
   * @param title - title of book
   * @param author - author of book
   * @param pages - number of pages of book
   * @param isbn - ISBN number of book
   * @param rating - rating of book
   */
  public Fiction(final String language, final String title,
      final String author, final int pages, final double isbn,
      final int rating) {
    super(language, title, author, pages, isbn);
    this.rating = rating;
  }

  /**
   * getter for rating.
   */
  public final int getRating() {
    return rating;
  }

  @Override
  public final void showParameters() {
    System.out.println(toString());
  }

  @Override
  public final String toString() {
    return "Fiction: " + super.toString()
        + ", rating=" + rating;
  }
}
