/*
 * Copyright (c) Vertsimaha Systems, Inc.
 * This software is part of home task
 */
package com.vertsimaha.menu;

import com.vertsimaha.filters.Service;
import java.util.Scanner;

public class Menu {
  private Service service;

  public Menu() {
    service = new Service();
  }

  public final void menu() {
    boolean check = false;
    Scanner choice = new Scanner(System.in);
    do {
      service.showMenu();
      int show = choice.nextInt();
      switch (show) {
        case 1:
          service.viewAllBooks();
          break;
        case 2:
          service.sortByType();
          break;
        case 3:
          service.sortByLanguage();
          break;
        case 4:
          service.sortByAuthor();
          break;
        case 5:
          service.searchByPages();
          break;
        case 6:
          check = true;
          break;
        default:
          break;
      }
    } while (!check);
  }
}
