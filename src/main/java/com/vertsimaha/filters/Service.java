/*
 * Copyright (c) Vertsimaha Systems, Inc.
 * This software is part of home task
 */
package com.vertsimaha.filters;

import com.vertsimaha.books.Book;
import java.util.Scanner;

/**
 * This class was created for implementation menu.
 *
 * @author Andrii Vertsimaha
 * @version 1.0
 * @since 2018-11-14
 */
public class Service {

  /**
   * Filter goes here.
   */
  private Filter filter;

  public Service() {
    filter = new Filter();
  }

  /**
   * Method shows Menu for user.
   */
  public final void showMenu() {
    System.out.print("1. View all books.\n"
        + "2. Sort by title.\n"
        + "3. Sort by language.\n"
        + "4. Sort by author.\n"
        + "5. Search by pages.\n"
        + "6. Exit\n"
        + "Make your choice: ");
  }

  /**
   * Method shows all books for user.
   */
  public final void viewAllBooks() {
    for (Book book : filter.getAllBooks()) {
      System.out.println(book);
    }
  }

  /**
   * Method sorts books by type.
   */
  public final void sortByType() {
    System.out.print("\nEnter type of book:\n"
        + "1. Lightfiction\n"
        + "2. Nonfiction\n"
        + "3. Fiction\n");
    Scanner choice = new Scanner(System.in);
    int typeNumber = choice.nextInt();
    switch (typeNumber) {
      case 1:
        for (Book book : filter.filterByType("Lightfiction")) {
          System.out.println(book);
        }
        break;
      case 2:
        for (Book book : filter.filterByType("Nonfiction")) {
          System.out.println(book);
        }
        break;
      default:
        for (Book book : filter.filterByType("Fiction")) {
          System.out.println(book);
        }
        break;
    }
  }

  /**
   * Method sorts books by language.
   */
  public final void sortByLanguage() {
    System.out.print("\nEnter language of book:\n"
        + "1. EN\n"
        + "2. PL\n"
        + "3. UA\n"
        + "4. LT\n");
    Scanner choice = new Scanner(System.in);
    int langNumber = choice.nextInt();
    switch (langNumber) {
      case 1:
        for (Book book : filter.filterByLanguage("EN")) {
          System.out.println(book);
        }
        break;
      case 2:
        for (Book book : filter.filterByLanguage("PL")) {
          System.out.println(book);
        }
        break;
      case 3:
        for (Book book : filter.filterByLanguage("UA")) {
          System.out.println(book);
        }
        break;
      case 4:
        for (Book book : filter.filterByLanguage("LT")) {
          System.out.println(book);
        }
        break;
      default:
        System.out.println("There are no books in this language");
        break;
    }
  }

  /**
   * Method sorts books by author.
   */
  public final void sortByAuthor() {
    System.out.print("\nEnter name of author: ");
    Scanner auth = new Scanner(System.in);
    String author = auth.nextLine();
    switch (author) {
      case "M.Bolton":
        for (Book book : filter.filterByAuthor("M.Bolton")) {
          System.out.println(book);
        }
        break;
      case "J.Rowling":
        for (Book book : filter.filterByAuthor("J.Rowling")) {
          System.out.println(book);
        }
        break;
      case "T.Shevchenko":
        for (Book book : filter.filterByAuthor("T.Shevchenko")) {
          System.out.println(book);
        }
        break;
      default:
        System.out.println("There are no books from this author!");
        break;
    }
  }

  /**
   * Method search books by number of pages.
   */
  public final void searchByPages() {
    System.out.print("\nEnter number of pages of book:\n");
    Scanner choice = new Scanner(System.in);
    int pages = choice.nextInt();
    if (filter.filterByPages(pages).isEmpty()) {
      System.out.println("There are no book with such number of pages!");
      filter.setBooks(null);
    } else {
      for (Book book : filter
          .filterByPages(pages)) {
        System.out.println(book);
      }
    }
  }
}
