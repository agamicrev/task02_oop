/*
 * Copyright (c) Vertsimaha Systems, Inc.
 * This software is part of home task
 */
package com.vertsimaha.filters;

import com.vertsimaha.books.Fiction;
import com.vertsimaha.books.Book;
import com.vertsimaha.books.Lightfiction;
import com.vertsimaha.books.Nonfiction;
import com.vertsimaha.creator.BooksCreator;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This class was created for filter functions with books.
 *
 * @author Andrii Vertsimaha
 * @version 1.0
 * @since 2018-11-14
 */
public class Filter {

  /**
   * List for containing books.
   */
  private List<Book> books;

  private BooksCreator booksCreator;

  public final void setBooks(final List<Book> books) {
    this.books = books;
  }

  public Filter() {
    booksCreator = new BooksCreator();
  }

  final List<Book> getAllBooks() {
    books = null;
    return booksCreator.getBooks();
  }

  public final List<Book> filterByAuthor(final String author) {
    if (books == null) {
      books = booksCreator.getBooks().stream()
          .filter(book -> book.getAuthor().equals(author))
          .sorted(Comparator.comparing(Book::getAuthor))
          .collect(Collectors.toList());
    } else {
      books = books.stream()
          .filter(book -> book.getAuthor().equals(author))
          .sorted(Comparator.comparing(Book::getAuthor))
          .collect(Collectors.toList());
    }
    return books;
  }

  public final List<Book> filterByLanguage(final String language) {
    if (books == null) {
      books = booksCreator.getBooks().stream()
          .filter(book -> book.getLanguage().equals(language))
          .sorted(Comparator.comparing(Book::getLanguage))
          .collect(Collectors.toList());
    } else {
      books = books.stream().filter(car -> car.getLanguage().equals(language))
          .sorted(Comparator.comparing(Book::getLanguage))
          .collect(Collectors.toList());
    }
    return books;
  }

  public final List<Book> filterByPages(final int pages) {
    if (books == null) {
      books = booksCreator.getBooks().stream()
          .filter(book -> book.getPages() >= pages)
          .sorted(Comparator.comparingInt(Book::getPages))
          .collect(Collectors.toList());
    } else {
      books = books.stream()
          .filter(book -> book.getPages() >= pages)
          .sorted(Comparator.comparingInt(Book::getPages))
          .collect(Collectors.toList());
    }
    return books;
  }

  public final List<Book> filterByType(final String type) {
    if (books == null) {
      switch (type) {
        case "Fiction":
          books = booksCreator.getBooks().stream()
              .filter(book -> book instanceof Fiction)
              .collect(Collectors.toList());
          break;
        case "Nonfiction":
          books = booksCreator.getBooks().stream()
              .filter(book -> book instanceof Nonfiction)
              .collect(Collectors.toList());
          break;
        case "Lightfiction":
          books = booksCreator.getBooks().stream()
              .filter(book -> book instanceof Lightfiction)
              .collect(Collectors.toList());
          break;
        default:
          books = booksCreator.getBooks().stream()
              .filter(book -> book instanceof Lightfiction)
              .collect(Collectors.toList());
          break;
      }
    } else {
      switch (type) {
        case "Fiction":
          books = books.stream()
              .filter(book -> book instanceof Fiction)
              .collect(Collectors.toList());
          break;
        case "Nonfiction":
          books = books.stream()
              .filter(book -> book instanceof Nonfiction)
              .collect(Collectors.toList());
          break;
        case "Lightfiction":
          books = booksCreator.getBooks().stream()
              .filter(book -> book instanceof Lightfiction)
              .collect(Collectors.toList());
          break;
        default:
          books = books.stream()
              .filter(book -> book instanceof Lightfiction)
              .collect(Collectors.toList());
          break;
      }
    }
    return books;
  }
}
