/**
 * Domain classes used to produce Menu.
 * <p>
 * These classes contain the Menu
 * </p>
 *
 * @author Andrii Vertsimaha
 * @version 1.0
 * @since 2018-11-14
 */
package com.vertsimaha.filters;